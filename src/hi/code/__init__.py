import sys
import logging
from pathlib import Path

from hi.vm.coreos import exec_subprocess_ssh
from hi.vm.vbox import vbox_forward_tcp, vbox_share_folder

logger = logging.getLogger(__name__)

code_server_container = 'code-server'
code_server_image = 'lscr.io/linuxserver/code-server:latest'
code_server_password = 'password'
code_server_password_sudo = 'password'
code_server_tz = 'Etc/GMT+2'
code_server_port = '8443'

def run_code_server_cmds():
	cmds = f'''sudo podman run -d \
--restart unless-stopped \
--privileged \
--replace \
--name={code_server_container} \
-e PUID=1000 \
-e PGID=1000 \
-e TZ={code_server_tz} \
-e PASSWORD={code_server_password} \
-e SUDO_PASSWORD={code_server_password_sudo} \
-e DEFAULT_WORKSPACE=/config/workspace \
-p {code_server_port}:8443 \
-v ./config:/config:z \
-v /var/run/docker.sock:/var/run/docker.sock \
{code_server_image}'''
	return cmds

def start_code_server():
	try:
		# TODO move out of start into create, but for now we asume it safe to fail
		exec_subprocess_ssh('sudo systemctl enable --now docker'.split('\n'))
		exec_subprocess_ssh('sudo docker version'.split('\n'))
		exec_subprocess_ssh('mkdir ./config'.split('\n'))
		exec_subprocess_ssh('sudo mkdir ./config/hi'.split('\n'))
		exec_subprocess_ssh('sudo mkdir ./config/embed'.split('\n'))
		exec_subprocess_ssh('sudo mount -t vboxsf hi_data ./config/hi'.split('\n'))
		exec_subprocess_ssh('sudo mount -t vboxsf hi_python ./config/embed'.split('\n'))
	except:
		result = ['Python exception check logs. It seem to fail at creating code server config directory or mounting vm shares.']
		logger.exception(cmds)
	try:
		result = exec_subprocess_ssh(run_code_server_cmds().split('\n'))
		if 'already in use' in ''.join(result):
			cmds = f'''sudo podman start {code_server_container}'''.split('\n')
			result = exec_subprocess_ssh(cmds)
			if code_server_container in ''.join(result):
				result = [f'{code_server_container} started on http://localhost:{code_server_port}.']
	except:
		result = ['Python exception check logs. Could not run or start coder server service.']
		logger.exception(cmds)
	return result

def code_server_forward_https(vm_name, host_port=code_server_port, vm_port=code_server_port):
	return vbox_forward_tcp(vm_name, 'codeserverhttps', host_port, vm_port)

def code_server_share_hi_data(vm_name, platform):
	hi_data_path = platform['config.boot.path']
	hi_python_path = Path(sys.executable).parent
	output1 = vbox_share_folder(vm_name, 'hi_data', hi_data_path)
	output2 = vbox_share_folder(vm_name, 'hi_python', hi_python_path)
	return True, [output1, output2]

def workspace_file_exist(filename):
	cmds = [f'ls ~/config/workspace/{filename}']
	try:
		return exec_subprocess_ssh(cmds)
	except:
		logger.exception(cmds)