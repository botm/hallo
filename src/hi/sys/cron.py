import sys
import time
import json
import os
from pathlib import Path

from .platform import get_platform

def main():
	basepath = Path(os.__file__).parent
	with open(f'{basepath}/hi.sys.cron.log', 'a') as f:
		now = time.gmtime()
		nowstr = time.strftime("%Y-%m-%d %H:%M:%S", now)
		print(f'[{nowstr}] {json.dumps(get_platform())}', file=f)

if __name__ == '__main__':
	sys.exit(main())