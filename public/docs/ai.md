# Algorithm Interface

Artificial intelligence [AI],
In its broadest sense, 
is intelligence exhibited by machines,
particularly computer systems.

It is a field of research in computer science 
that develops and studies methods and software 
that enable machines to perceive their environment 
and use learning and intelligence to take actions 
that maximize their chances of 
achieving defined goals.

Such machines may be called AIs. 

[AI]: https://en.wikipedia.org/wiki/Artificial_intelligence