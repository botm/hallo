<!-- iso: afr -->
# luislang

```
Ons is wat ons gebruik,
dis moet ons soos 'n lui,
akkedis op 'n rots sit,
ons omgewing obsiveer
en integreer.
```

# integreer

`luislang`/python kan gesien word as 'n turning kompleet algemene programeertaal,
dus ons "vm runtime".

gewoonlik kom die vm op enige posix beheerstelsel gebruik word bv,
`apt install python`/`dnf install python`(debian/rhel)
`python

# vensters en planete

Vir Windows word daar 'n "embed" vm gebou wat net afgelaai en gebruik kan word.
Gebruik `<ctrl>+r` vir "Run" `powershell.exe` en kopie, paste die volgende:

```ps1
# laai a powershell script af en install python in %appdata%\python
powershell -c "[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12; Invoke-WebRequest -Uri 'https://gitlab.com/botm/hallo/-/raw/main/scripts/install-py.ps1' -OutFile $env:appdata\install-py.ps1; cd $env:appdata; powershell .\install-py.ps1"
# die boonste ps1 script instaleer ook pip en verander _pth om site en site-packages by-tevoeg
& $env:appdata\python\python.exe -m pip install jupyterlab
cd $env:appdata\python\Scripts
jupyter lab
```

Dit behoort jou 'n werkende `jupyter` omgewing te gee wat leef in

```cmd
%appdata%\python
```

Bv maak die boonste "path" in `ctrl+r` "Run" oop,
om die omgewing in `explorer.exe` te sien.