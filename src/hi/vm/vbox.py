import os
import logging
import json
import urllib.request

from hi.sys.platform import exec_subprocess

logger = logging.getLogger(__name__)

def download_url_text(url, except_return=None):
	try:
		with urllib.request.urlopen(url) as response:
			return response.read().decode("utf-8")
	except urllib.error.URLError as e:
		logger.error(f"Error downloading '{url}': {e}")
		return except_return

def download_vbox_disk(vbox_disk_url, output_file):
	try:
		with urllib.request.urlopen(vbox_disk_url) as response:
			with open(output_file, "wb") as f:
				f.write(response.read())
		logger.debug(f"Downloaded VirtualBox disk to {output_file}")
	except urllib.error.URLError as e:
		logger.error(f"Error downloading VirtualBox disk: {e}")

def vbox_add_path(platform=None, config=None):
	virtualbox_path = "C:\\myVirtualBox"
	current_path = os.environ.get('PATH', '')
	if virtualbox_path not in current_path:
		os.environ['PATH'] += f";{virtualbox_path}"
		logger.debug(f"{virtualbox_path} added to PATH")
	else:
		logger.debug(f"{virtualbox_path} is already in PATH")

def vbox_import(vm_name, ova_file):
	'Set the VM name and OVA file path'
	# Run VBoxManage import command
	output = exec_subprocess(cmds=["VBoxManage", "import", "--vsys", "0", "--vmname", vm_name, ova_file])
	logger.debug(f"Imported {vm_name} from {ova_file} result:\n{output}")
	if 'already exists' in output:
		return True, output
	elif 'Error' in output:
		return False, output
	return True, output

def vbox_ignite(vm_name, ign_conf):
	output = exec_subprocess(cmds=["VBoxManage", "guestproperty", "set", vm_name, "/Ignition/Config", ign_conf])
	return True, output

def vbox_forward_tcp(vm_name, forward_name, host_port, vm_port):
	output = exec_subprocess(cmds=["VBoxManage", "modifyvm", vm_name, "--natpf1", f'"{forward_name},tcp,,{host_port},,{vm_port}"'])
	return True, output

def vbox_share_folder(vm_name, name, host_path):
	output = exec_subprocess(cmds=["VBoxManage", "sharedfolder", "add", vm_name, "--name", name, "--hostpath", host_path, "--automount"])
	return True, output

def vbox_forward_ssh(vm_name, host_port=2223, vm_port=22):
	return vbox_forward_tcp(vm_name, 'guestssh', host_port, vm_port)

def vbox_start(vm_name):
	output = exec_subprocess(cmds=["VBoxManage", "startvm", vm_name])
	return True, output

def vbox_delete(vm_name):
	output = exec_subprocess(cmds=["VBoxManage", "unregistervm", vm_name, '--delete'])
	return True, output
	
def vbox_poweroff(vm_name):
	output = exec_subprocess(cmds=["VBoxManage", "controlvm", vm_name, 'poweroff'])
	return True, output