import configparser
import platform
import subprocess
import sys
import os
import logging

logger = logging.getLogger(__name__)

def expand_config_path(platform):
	if 'Windows' in platform['platform.platform']:
		config_path = f"{os.getenv('APPDATA')}/hi"
	else:
		config_path = f"{os.path.expanduser('~')}/.hi"
	logger.debug(f'platform expanded config path: {config_path}')
	return config_path

def get_platform(result={}, config=None):
	def add(k, vcb):
		try:
			result[k] = vcb()
		except:
			logger.exception(f'k={k}, vcb={vcb}')
	add('sys.version', lambda: sys.version)
	add('platform.platform', platform.platform)
	try:
		add('platform.system', platform.system)
		add('platform.machine', platform.machine)
		add('platform.uname', platform.uname)
		add('platform.version', platform.version)
		add('platform.mac_ver', platform.mac_ver)
		add('os.getlogin', os.getlogin)
		add('os.path.home', lambda: os.path.expanduser('~'))
	except:
		logger.warning('limited platform information')
	if config:
		try:
			config_path = config.get('boot', 'path')
		except configparser.NoSectionError:
			config_path = expand_config_path(result)
	else:
		config_path = expand_config_path(result)
	add('config.boot.path', lambda: config_path)
	return result

def exec_subprocess(cmds):
	try:
		logger.debug(f'Args: \n{cmds}')
		completed_process = subprocess.run(cmds, capture_output=True, text=True, check=True)
		return completed_process.stdout
	except subprocess.CalledProcessError as e:
		return f"Error executing script: {e.stderr}"