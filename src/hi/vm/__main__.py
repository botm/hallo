import argparse
import sys

from .vbox import vbox_forward_tcp

def main(platform=None, config=None):
	#print(install_coreos(platform, config))
	parser = argparse.ArgumentParser(description="Forward TCP ports for VirtualBox VMs")
	parser.add_argument("vm_name", help="Name of the VirtualBox VM")
	parser.add_argument("forward_name", help="Name for the port forwarding rule")
	parser.add_argument("host_port", type=int, help="Host port to forward")
	parser.add_argument("vm_port", type=int, help="VM port to forward to")
	args = parser.parse_args()
	vbox_forward_tcp(args.vm_name, args.forward_name, args.host_port, args.vm_port)
	return 0

if __name__ == "__main__":
	sys.exit(main())