import logging

logger = logging.getLogger(__name__)

def mikrotik_routeros_secure_admin(host="192.168.88.1",username="admin",password="",new_password=None):
	from netmiko import ConnectHandler
	# Define router details
	mikrotik = {
		"device_type": "mikrotik_routeros",
		"host": host,
		"username": username,
		"password": password,
	}

	# Connect to the router
	with ConnectHandler(**mikrotik) as net_connect:
		if not new_password:
			import datetime
			new_password  = datetime.date.today().year
		# Change the password
		cmd = f"/user set 0 password={new_password}"
		logger.debug(cmd)
		net_connect.send_command(cmd)