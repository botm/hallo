import sys
import time
import uuid
import threading

from hi.sys.platform import get_platform

try:
	config = config
except:
	import configparser
	config = configparser.ConfigParser()

platform = get_platform(config=config)

try:
	user = platform["os.getlogin"]
except:
	user = f'Anon{str(uuid.uuid1()).split('-')[0]}'

now = time.gmtime()
nowstr = time.strftime("%Y-%m-%d %H:%M:%S", now)
print(f'[{nowstr}] Hi {user}', file=sys.stderr)

if 'Windows' in platform['platform.platform']:
	from hi.web import main as web_server
	def browser_open(url):
		import webbrowser
		from time import sleep
		sleep(3)
		webbrowser.open_new_tab(url)
	frontend = threading.Thread(target=browser_open,args=('http://localhost:8080',)) 
	backend = threading.Thread(target=web_server,args=(platform, config,))
	try:
		frontend.start()
		backend.run()
	except KeyboardInterrupt:
		sys.exit('KeyboardInterrupt')