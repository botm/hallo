from pydantic_settings import BaseSettings, SettingsConfigDict
from pydantic import AnyUrl 

class Config(BaseModel):
	config_file: str = 'config.yaml'
	fallback_url: AnyUrl ='http://botm.gitlab.io/hallo/hi.sys/boot.py'
	debug: bool = True

class Settings(BaseSettings):
	model_config = SettingsConfigDict(env_prefix='hi') 
	config: Config = Config()