import logging
import urllib.request
import re

from hi.sys.platform import exec_subprocess

logger = logging.getLogger(__name__)

def exec_ps1(ps1src):
	"""
	Execute a PowerShell script.

	Args:
		ps1src (str): The PowerShell script source code.

	Returns:
		str: Output (stdout) from the PowerShell execution.
	"""
	# Run PowerShell script
	return exec_subprocess(cmds=["powershell", "-Command", ps1src])


def web_view(nav_url='http://botm.gitlab.io/hallo/hi.sys/'):
	# Your PowerShell script here
	ps1_script = '''Add-Type -AssemblyName System.Windows.Forms
# Create a new form
$form = New-Object Windows.Forms.Form
$form1.Text = "Hi System!!1"
$form.WindowState = [System.Windows.Forms.FormWindowState]::Maximized
# Create a web browser control
$webView = New-Object Windows.Forms.WebBrowser
$webView.Dock = [System.Windows.Forms.DockStyle]::Fill
$form.Controls.Add($webView)
# Handle the form's Resize event
$form.add_Resize({
	$webView.Size = $form.ClientSize
})
# Navigate to the desired URL
$webView.Navigate("''' + nav_url + '''")
# Show the form
$form.ShowDialog()
	'''
	output = exec_ps1(ps1_script)
	logger.debug(output)

def ps1_download(uri, out, exec=False):
	cmds = f"""powershell -c "$url='{uri}';$path='{out}';""" + \
		'''if(-not(Test-Path -Path $path)){[Net.ServicePointManager]::SecurityProtocol=[Net.SecurityProtocolType]::Tls12;Invoke-WebRequest ''' + \
		'''-Uri $url -OutFile $path}'''
	if exec:
		cmds += f''';{exec}"'''
	else:
		cmds += f'''"'''
	return exec_subprocess(cmds)

def install_vm(platform=None):
	outpath = '$env:appdata/hi'
	if platform:
		try:
			outpath = platform['config.boot.path']
		except:
			logger.exception('install vm to user roaming directory')
	# Step 1: Download the content of LATEST.TXT
	latest_version_url = "http://download.virtualbox.org/virtualbox/LATEST.TXT"
	with urllib.request.urlopen(latest_version_url) as response:
		latest_version = response.read().decode("utf-8").strip()
	# Step 2: Download the content for the latest version
	download_url = f"http://download.virtualbox.org/virtualbox/{latest_version}/"
	with urllib.request.urlopen(download_url) as response:
		content = response.read().decode("utf-8")
	logger.debug(f"Searching for anchor href pattern in [{content}]")
	# Step 3: Search for the link containing "VirtualBox-*-Win.exe"
	pattern = r'<a href="(VirtualBox-\d+\.\d+\.\d+-\d+-Win\.exe)">'
	match = re.search(pattern, content)
	if match:
		# Step 4: Extract the href value
		virtualbox_exe_link = match.group(1)
		logger.debug(f"Found VirtualBox executable link: {virtualbox_exe_link}")
	else:
		msg = "VirtualBox executable link not found."
		logger.error(msg)
		return False, msg
	exe_download_url = f"{download_url}{virtualbox_exe_link}"
	cpp_redist_url = "https://aka.ms/vs/17/release/vc_redist.x64.exe"
	install_script_url = "https://gitlab.com/botm/hallo/-/raw/main/scripts/install-vm.bat"
	logger.debug(f'Download and execute vm install scripts: {exe_download_url} | {cpp_redist_url} | {install_script_url}')
	exec_ps1(f'mkdir -p {outpath}')
	output0 = ps1_download(uri=exe_download_url, out=f'{outpath}/VirtualBox.installer.exe')
	output1 = ps1_download(uri=cpp_redist_url, out=f'{outpath}/vc_redist.x64.exe', exec=f'cd {outpath}; ./vc_redist.x64.exe --quiet --includeRecommended')
	output2 = ps1_download(uri=install_script_url, out=f'{outpath}/install-vm.bat', exec=f'cd {outpath}; ./install-vm.bat')
	return True, output0, output1, output2