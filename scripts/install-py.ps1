$version = "3.12.4" # python version
$name = "python-$version-embed-amd64"
$zip = "$name.zip" 
$uri = "https://www.python.org/ftp/python/$version/$zip"
$uri_pip = "https://bootstrap.pypa.io/get-pip.py"
$dest = "./$name"
if (!$args[0]) { } else { $dest = $args[0] }
$dest = $ExecutionContext.SessionState.Path.GetUnresolvedProviderPathFromPSPath($dest)
if (Test-Path $dest) { # remove prev install if exist
	rm -r $dest
}
mkdir $dest
if (!(Test-Path $zip)) { # if python embed zip not present download
	Invoke-WebRequest -UseBasicParsing -OutFile $zip -Verbose -Uri $uri	
}
Expand-Archive -Path $zip -DestinationPath $dest
# patch import site and site-packages into embed env
$patch_site = "$dest\python312._pth"
Add-Content -Path $patch_site -Value 'import site'
Add-Content -Path $patch_site -Value 'Lib/site-packages'
# install pip
if (!(Test-Path ./pip.py)) { # if pip.py not present download in $dest
	Invoke-WebRequest -UseBasicParsing -OutFile $dest\pip.py -Verbose -Uri $uri_pip
	& $dest\python.exe $dest\pip.py
} else {
	& $dest\python.exe pip.py
}