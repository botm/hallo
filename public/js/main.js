/** my fever dream of sorrow, rise again, 
`<p><strong>South Africa</strong>:</p>
<ul>
<li><strong>Afrikaans</strong>: Hallo</li>
<li><strong>English</strong>: Hi</li>
<li><strong>Sesotho</strong>: Dumela (singular) / Dumelang (plural)</li>
<li><strong>isiZulu</strong>: Sawubona! (singular) / Sanibona! (plural)</li>
<li><strong>isiNdebele</strong>: Lotjhani / Salibonani</li>
<li><strong>isiXhosa</strong>: Molo (singular) / Molweni (plural)</li>
<li><strong>Setswana</strong>: Dumela (singular) / Dumelang (plural)</li>
<li><strong>Tshivenda</strong>: Ndaa (men) / Aa (women)</li>
<li><strong>Xitsonga</strong>: Abuxeni</li>
</ul>
<p><strong>Zimbabwe</strong>:</p>
<ul>
<li><strong>Shona</strong>: Kanoi</li>
</ul>
<p><strong>Namibia</strong>:</p>
<ul>
<li><strong>German</strong>: Hallo</li>
</ul>
<p><strong>Mozambique</strong>:</p>
<ul>
<li><strong>Portuguese</strong>: Olá</li>
</ul>
</div>`
*/
//taal = "<li><strong>English</strong>: Hi</li>"
/** Omitting any language could inadvertently marginalize or offend speakers of that language, 
 * so it’s crucial to recognize and celebrate the rich linguistic tapestry of Southern Africa,
 * to further language support please reach out */
//document.write(taal)