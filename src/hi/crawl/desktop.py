import os
import re
import logging
from time import sleep

try:
	import pyautogui
except:
	raise Exception('pip install pyautogui pillow opencv-python')

logger = logging.getLogger(__name__)

s1 = 0.8
s3 = 1.8

img_path = './crawl/desktop/'

def set_img_path_prefix(platform=None):
	global img_path
	prefix = '.' if platform is None else platform['config.boot.path']
	img_path = f'{prefix}/crawl/desktop/'

def find_img_center(src, grayscale=True, confidence=0.7):
	global img_path
	src = img_path + src
	if os.path.isfile(src):
		try:
			xy = pyautogui.locateCenterOnScreen(src, grayscale=grayscale, confidence=confidence)
		except:
			logger.exception(f'Exception with locating: {src}')
			return None
		return xy
	else:
		logger.warning(f'Could not locate: {src}')
		return None

def click_img_center(src):
	xy = find_img_center(src)
	if xy:
		pyautogui.click(xy[0], xy[1])
	else:
		raise Exception(f'Could not click: {src}')

def ai_prompt_to_copy(prompt, clean=True, shift_enter=False, interval=0.1):
	if clean:
		prompt = re.sub(r"[\t\n\r]*", "", prompt)
	visible = find_img_center('browser-visible.png')
	if not visible:
		click_img_center(src='taskbar-browser.png')
		sleep(s1)
	click_img_center(src='browser-open-tab.png')
	sleep(s1)
	click_img_center(src='ai.png')
	sleep(s3)
	click_img_center(src='ai-prompt.png')
	sleep(s3)
	click_img_center(src='ai-textarea.png')
	sleep(s3)
	pyautogui.write(prompt, interval=interval)
	if shift_enter:
		sleep(s1)
		pyautogui.keyDown('shift')
		pyautogui.keyDown('enter')
		sleep(0.1)
		pyautogui.keyUp('shift')
		pyautogui.keyUp('enter')
	else:
		click_img_center(src='ai-submit.png')
	sleep(s3)
	waiting = True
	while waiting:
		found = find_img_center('ai-stop.png')
		if found:
			logger.debug('waiting')
			sleep(s1)
		else:
			waiting = False
	sleep(s1)
	click_img_center(src='ai-copy.png')

def code_server_paste(filename,interval=0.1):
	click_img_center(src='code-server.png')
	sleep(s1)
	click_img_center(src='code-server-menu.png')
	sleep(s1)
	click_img_center(src='code-server-file.png')
	sleep(s1)
	click_img_center(src='code-server-file-new.png')
	sleep(s1)
	pyautogui.hotkey('ctrl', 'v') 
	sleep(s1)
	pyautogui.hotkey('ctrl', 's') 
	sleep(s1)
	pyautogui.write(filename, interval=interval)
	sleep(s1)
	pyautogui.keyDown('enter')
	sleep(0.1)
	pyautogui.keyUp('enter')