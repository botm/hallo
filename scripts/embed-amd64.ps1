# install embed python
$version = "3.12.4"
$name = "python-$version-embed-amd64"
$zip = "$name.zip"
$uri = "https://www.python.org/ftp/python/$version/$zip"
# get pip
$uri_pip = "https://bootstrap.pypa.io/get-pip.py"
# get hello helper package
$uri_hi = "https://gitlab.com/api/v4/projects/59092300/packages/generic/hi/0.1.0/hi-0.1.0.tar.gz"
# our ve default would be $env:appdata\python\python.exe
$dest = ".\python"
# allow arg override of install dest
if (!$args[0]) { } else { $dest = $args[0] }
$confirmation = 'y'
# expand $dest
$dest = $ExecutionContext.SessionState.Path.GetUnresolvedProviderPathFromPSPath($dest)
$patch_site = "$dest\python312._pth"
if (Test-Path $dest) {
	$confirmation = Read-Host "'$dest' already exist, destroy and recreate?"
	if ($confirmation -eq 'y') {
		rm -r $dest
	}
}
if ($confirmation -eq 'y') {
	mkdir $dest
	# download python embed env
	if (!(Test-Path $zip)) {
		Invoke-WebRequest -UseBasicParsing -OutFile $zip -Verbose -Uri $uri
	}
	Expand-Archive -Path $zip -DestinationPath $dest
	# download pip
	Invoke-WebRequest -UseBasicParsing -OutFile $dest\get-pip.py -Verbose -Uri $uri_pip
	# add site packages to embed env
	Add-Content -Path $patch_site -Value 'Lib/site-packages'
	# install deps
	& $dest\python.exe $dest\get-pip.py setuptools bottle pydantic pydantic-settings netmiko zabbix_utils
	# create task
	schtasks /create /tn "hi.sys" /tr "$dest\pythonw.exe -m hi.sys.cron" /sc minute /mo 1
}
# install/update hi.sys
& $dest\python.exe $dest\Scripts\pip.exe install --upgrade --force-reinstall $uri_hi
# exec hi.sys
& $dest\python.exe -m hi.sys