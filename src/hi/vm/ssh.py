import os
import subprocess
import logging

logger = logging.getLogger(__name__)

def create_ssh_key():
	ssh_dir = os.path.expanduser("~/.ssh")
	id_rsa_path = ssh_key_path()
	id_rsa_pub_path = os.path.join(ssh_dir, "id_rsa.pub")

	if os.path.exists(id_rsa_pub_path):
		print(f"{id_rsa_pub_path} already exists.")
	elif os.path.exists(id_rsa_path):
		# Generate the public key from the private key
		subprocess.run(["ssh-keygen", "-y", "-f", id_rsa_path], stdout=open(id_rsa_pub_path, "w"))
		print(f"Created {id_rsa_pub_path} from {id_rsa_path}.")
	else:
		# Generate both private and public keys
		subprocess.run(["ssh-keygen", "-t", "rsa", "-N", "", "-f", id_rsa_path])
		print(f"Created {id_rsa_path} and {id_rsa_pub_path}.")
	if os.path.exists(id_rsa_pub_path):
		with open(id_rsa_pub_path, "r") as pub_key_file:
			pub_key_content = pub_key_file.read()
	return pub_key_content

def ssh_key_path():
	ssh_dir = os.path.expanduser("~/.ssh")
	id_rsa_path = os.path.join(ssh_dir, "id_rsa")
	return id_rsa_path