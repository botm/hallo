import unittest
import sys
import io
import json

from hi.sys.__main__ import main

class TestPlatformInfo(unittest.TestCase):
	def setUp(self):
		# Redirect stdout to a StringIO object
		self.stdout = io.StringIO()
		sys.stdout = self.stdout

	def tearDown(self):
		# Restore original stdout
		sys.stdout = sys.__stdout__

	def test_platform_info(self):
		# Call the get_platform function
		main()

		# Capture the stdout content
		stdout_content = self.stdout.getvalue()

		# Parse the JSON object
		try:
			platform_info = json.loads(stdout_content)
		except json.JSONDecodeError:
			self.fail("Failed to parse JSON output")

		# Check if the keys are present
		self.assertIn("sys.version", platform_info, "sys.version key not found")
		self.assertIn("platform.platform", platform_info, "platform.platform key not found")

if __name__ == "__main__":
	unittest.main()