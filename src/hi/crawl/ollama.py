import urllib.request
import json

ollama_generate_url = "http://localhost:11434/api/generate"
ollama_model = "llama2"

def ai_prompt(prompt):
	data = {
		"model": ollama_model,
		"prompt": prompt,
		"stream": False
	}
	headers = {
		'Content-Type': 'application/json'
	}
	req = urllib.request.Request(ollama_generate_url, data=json.dumps(data).encode('utf-8'), headers=headers)
	with urllib.request.urlopen(req) as response:
		result = response.read().decode('utf-8')
		return result