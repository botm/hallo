:: Credit https://gist.github.com/badrelmers/20990cd518fed05e5f171961c63e85ba
@echo off
setlocal
pushd %~pd0

:: ###################################################################
:: please configure this if your VirtualBox exe have a different name
:: ###################################################################
set "VirtualBox_installer=VirtualBox.installer.exe"

:: ###################################################################
:: from here you do not need to change anything
:: ###################################################################
set "install_dir=c:\myVirtualBox"

echo.
IF NOT EXIST "%VirtualBox_installer%" ( color 4F & echo. & echo %VirtualBox_installer% was not found, if you downloaded a new installer or changed its name then please change this script to point to the new installer, you have to change VirtualBox_installer line. & echo. & exit /b)

set "portable_dir_name=myVMBOX"
set "VBOX_symlink=c:\%portable_dir_name%"
set "VBOX_Real=%~dp0%portable_dir_name%"
mkdir "%VBOX_Real%" 2>nul

rem never use rd /S it will delete the real dir content if it was created with junction.exe in xp
IF EXIST "%VBOX_symlink%" rd /Q "%VBOX_symlink%"
mklink /J "%VBOX_symlink%" "%VBOX_Real%" >nul

setx VBOX_USER_HOME c:\%portable_dir_name% >nul

set "VBOX_USER_HOME=c:\%portable_dir_name%"

IF NOT EXIST "%install_dir%\VirtualBox.exe" call :install

"%install_dir%\VBoxManage.exe" setproperty machinefolder "c:\%portable_dir_name%\Machines"

echo vm.start="%install_dir%\VirtualBox.exe"

exit /b

:install
    tasklist /fi "ImageName eq VirtualBox.exe" /fo csv 2>NUL | find /I "VirtualBox.exe">NUL
    if "%ERRORLEVEL%"=="0" set dirty_install=yes
    tasklist /fi "ImageName eq VBoxSDS.exe" /fo csv 2>NUL | find /I "VBoxSDS.exe">NUL
    if "%ERRORLEVEL%"=="0" set dirty_install=yes
    tasklist /fi "ImageName eq VBoxSVC.exe" /fo csv 2>NUL | find /I "VBoxSVC.exe">NUL
    if "%ERRORLEVEL%"=="0" set dirty_install=yes

    IF EXIST "%ProgramFiles%\Oracle\VirtualBox\VirtualBox.exe" set dirty_install=yes
    IF EXIST "%ProgramFiles(x86)%\Oracle\VirtualBox\VirtualBox.exe" set dirty_install=yes
    
    if [%dirty_install%]==[yes] ( 
        color 4F & echo.
        echo   VirtualBox is already installed or you are running a portable virtualbox. 
        echo   only one virtualbox can run at the same time, and there should not be more
        echo   than one installed virtualbox. if you have virtualbox installed please 
        echo   uninstall it first, and if you are running a Portable virtualbox then stop
        echo   it first, and run this script again. exiting...
        echo. & exit 
    )
    
    echo installing VirtualBox, please wait...
    "%VirtualBox_installer%" --silent --ignore-reboot --msiparams "INSTALLDIR=""%install_dir%""" VBOX_INSTALLDESKTOPSHORTCUT=0 VBOX_INSTALLQUICKLAUNCHSHORTCUT=0 VBOX_START=0
    IF NOT EXIST "%install_dir%\VirtualBox.exe" ( color 4F & echo. & echo %install_dir%\VirtualBox.exe was not found, VirtualBox could not be installed. exiting... & echo. & exit )

    echo VirtualBox was installed
exit /b