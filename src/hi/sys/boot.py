import sys
import configparser
import urllib.request
import logging

logger = logging.getLogger(__name__)

def debug_to_stderr():
	logging.basicConfig(stream=sys.stderr, level=logging.DEBUG)

def exec_config(
		config_file='config.ini',
		fallback_url='http://botm.gitlab.io/hallo/hi.sys/boot.py',
		debug=True,
		config_inject=True # WHY!?!?
	):
	if debug:
		debug_to_stderr()
	config = configparser.ConfigParser()
	config.read(config_file)
	try:
		url = config.get('boot', 'url')
	except (configparser.NoSectionError, configparser.NoOptionError):
		url = fallback_url
	try:
		# Download the content from the URL
		response = urllib.request.urlopen(url)
		content = response.read().decode('utf-8')
		with open('./boot.config.py', 'w') as f:
			f.write(content)
	except:
		logger.exception(f"Error downloading content from: {url}")
		try:
			with open('./boot.config.py', 'r') as f:
				content = f.read()
		except:
			logger.exception(f"could not fallback to previous boot config, exiting.")
			return 2
	# Evaluate the content (use with caution!)
	try:
		if config_inject:
			inject_src = f'''import configparser
config = configparser.ConfigParser()
config.read("{config_file}")
'''
			content = inject_src + content
		exec(content)
	except:
		logger.exception(f"Error eval bootstrap content: \n{content}")
		return 1
	return 0

def set_config(section, key, val, config_file='config.ini'):
	config = configparser.ConfigParser()
	config.read(config_file)
	config.set(section, key, val)