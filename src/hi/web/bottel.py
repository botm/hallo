"""
Bottel Web Raamwerk!1
Module gee minimum funksies om `bottle` te na-aap, 
maar gebruik `http.server`.
"""
import http.server
import socketserver

_routes = {}
_names = {}

class BottelRequestHandler(http.server.SimpleHTTPRequestHandler):
	def do_GET(self):
		if _routes.get(self.path) and _routes[self.path].get('GET'):
			call = _routes[self.path].get('GET')
			result = call()
			self.send_response(200)
			self.send_header('Content-type', 'text/html')
			self.end_headers()
			self.wfile.write(result)

def route(path: str, method: str = 'GET', callback: callable = None, name: str = None) -> None`:
	'''
	Binds a Python function to a specific URL path.

	Args:
		path (str): The URL path (e.g., '/hello').
		method (str, optional): The HTTP method (default is 'GET').
		callback (callable, optional): The Python function that handles requests to the specified path.
		name (str, optional): Assigns a name to the route for reference.

	Example:
		@route('/hello')
		def hello() -> str:
			return "Hello Wêreld!"
	'''
	route = _routes.get(path, dict())
	call = route.get(method, dict(c=callback))
	if name:
		call['n'] = name
		_names[name] = call
	route[method] = call
	_routes[path] = route

def run(host: str = 'localhost', port: int = 8080, debug: bool = False, reloader: bool = False, server: str = 'wsgiref') -> None`:
	'''
	Starts a built-in development server to serve your Bottle application.

	Args:
		host (str, optional): The hostname or IP address (default is 'localhost').
		port (int, optional): The port number (default is 8080).
		debug (bool, optional): Enables debugging mode (default is False).
		reloader (bool, optional): Automatically reloads the server on code changes (default is False).
		server (str, optional): Specifies the backend server (default is 'wsgiref').

	Example:
		run(host='localhost', port=8080, debug=True)
	'''
	with socketserver.TCPServer((host, port), BottelRequestHandler) as httpd:
		print(f'Serving on port {port}...')
		httpd.serve_forever()


def template(template_name: str, **kwargs) -> str`:
	'''
	Renders an HTML template using the specified template engine (Jinja2 by default).

	Args:
		template_name (str): The name of the template file (e.g., 'my_template.tpl').
		kwargs: Keyword arguments passed to the template for dynamic content.

	Example:
		@route('/hello/<name>')
		def index(name: str = 'World') -> str:
			return template('<b>Hello {{name}}</b>', name=name)
	'''
	pass

def static_file(filename: str, root: str = '.', mimetype: str = None, download: bool = False, charset: str = 'utf-8') -> str`:
	'''
	Serves static files (e.g., CSS, JavaScript, images) from a specified directory.

	Args:
		filename (str): The name of the file to serve (e.g., 'style.css').
		root (str, optional): The directory where static files are located (default is the current directory).
		mimetype (str, optional): Sets the MIME type for the response.
		download (bool, optional): Forces the browser to download the file.

	Example:
		@route('/css/<filename>')
		def stylesheets(filename: str) -> str:
			return static_file(filename, root='./static/css/')
	'''
	pass

'''
class MyRequestHandler(http.server.SimpleHTTPRequestHandler):
	def do_GET(self):
		if self.path == '/':
			pass

		elif self.path == '/static':
			# Serve a static file (e.g., index.html)
			self.send_response(200)
			self.send_header('Content-type', 'text/html')
			self.end_headers()
			with open('index.html', 'rb') as f:
				self.wfile.write(f.read())

		elif self.path == '/redirect':
			# Redirect to another URL
			self.send_response(302)
			self.send_header('Location', 'https://www.example.com')
			self.end_headers()

		else:
			# Handle other routes
			self.send_response(404)
			self.send_header('Content-type', 'text/html')
			self.end_headers()
			self.wfile.write(b'Page not found!')

if __name__ == '__main__':
	PORT = 8080
''''