from setuptools import setup, find_packages

setup(
	name='hi',
	version='0.1.0',
	packages=find_packages(),
	license='BSD-3',
	description='Hallo Programmatuur(Hi Agent)',
	package_data = {"hi.web": ["**/*.html", "**/*.css", "**/*.js", "**/*.png"]},
	include_package_data = True,
)