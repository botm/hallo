import json
import sys

from .platform import get_platform
from .boot import exec_config

def main():
	print(json.dumps(get_platform()))
	if len(sys.argv) > 1:
		return exec_config(sys.argv[1])
	else:
		return exec_config()

if __name__ == '__main__':
	sys.exit(main())