import uuid
import json
from importlib.resources import files

from hi.web.server import route, run, template, static_file, redirect, request
from hi.sys.platform import get_platform
from hi.schema import Details, ProblemDetails, ErrorDetail, DetailsData

_platform = None
_config = None

@route('/')
def serve_index():
	return redirect('/static/index.html')

@route('/static/<filename:path>')
def serve_static(filename):
	return static_file(filename, root=files('hi.web').joinpath('static'))

@route('/hi.sys')
def serve_api():
	global _platform # i hate myself
	try:
		user = _platform["os.getlogin"]
	except:
		user = f'Anon{str(uuid.uuid1()).split('-')[0]}'

	return template('<b>Hallo {{name}}</b> <a href="">click Here</a> to setup ur system!', name=user)

@route('/hi.sys/install/vm')
def serve_api_install_vm():
	global _platform
	from hi.windows.ps1 import install_vm
	result = install_vm(_platform)
	if len(result) >= 4 and 'vm.start' in result[3]:
		key, exe_location = result[3].split('=')
		return Details(
			detail=f'Virtual machine installed at "{exe_location}".').json()
	else:
		errors = []
		for idx, line in enumerate(result):
			errors.append(ErrorDetail(code=str(idx),detail=str(line))) # miss using code for line idx already tsk tsk tsk
		return ProblemDetails(
			type="https://gitlab.com/botm/hallo/-/blob/main/public/docs/api.md#VM+Install+Failed",
			title="Failed",
			detail=f'Virtual machine failed to install.',errors=errors).json()

@route('/hi.sys/install/coreos')
def serve_api_install_coreos():
	global _platform
	from hi.vm.coreos import install_coreos
	result = install_coreos(_platform)
	return Details(
		detail=f'{result}').json()

@route('/hi.sys/start/coreos')
def serve_api_start_coreos():
	global _platform
	from hi.vm.coreos import start_coreos
	result = start_coreos(_platform)
	return Details(
		detail=f'{result}').json()

@route('/hi.sys/stop/coreos')
def serve_api_stop_coreos():
	global _platform
	from hi.vm.coreos import stop_coreos
	result = stop_coreos(_platform)
	return Details(
		detail=f'{result}').json()

@route('/hi.sys/delete/coreos')
def serve_api_start_coreos():
	global _platform
	from hi.vm.coreos import delete_coreos
	result = delete_coreos(_platform)
	return Details(
		detail=f'{result}').json()

@route('/hi.sys/dms/start')
def serve_api_start_dms():
	global _platform
	from hi.dms.monitor import start_server
	result = start_server() # _platform
	return Details(
		detail=f'{result}').json()

@route('/hi.sys/code/server/start')
def serve_api_code_server_start():
	global _platform
	from hi.code import start_code_server
	result = start_code_server() # _platform
	return Details(
		detail=f'{result}').json()

@route('/hi.sys/crawl/desktop/ai2code', method='POST')
def serve_api_crawl_desktop_ai2code():
	global _platform
	from hi.crawl.desktop import ai_prompt_to_copy, code_server_paste, set_img_path_prefix
	from hi.code import workspace_file_exist
	prompt = request.body.read().decode(encoding="utf-8")
	filename = request.query.get('filename')
	set_img_path_prefix(_platform)
	ai_prompt_to_copy(prompt)
	code_server_paste(filename)
	result = workspace_file_exist(filename)
	return Details(
		detail=f'{result}').json()

@route('/hi.sys/crawl/ollama/prompt', method='POST')
def serve_api_crawl_ollama_prompt():
	global _platform
	from hi.crawl.ollama import ai_prompt
	from hi.code import workspace_file_exist
	prompt = request.body.read().decode(encoding="utf-8")
	filename = request.query.get('filename')
	result = json.loads(ai_prompt(prompt))
	if filename:
		from hi.windows.clipboard import set_clipboard_text
		from hi.crawl.desktop import code_server_paste, set_img_path_prefix
		set_clipboard_text(result['response'])
		set_img_path_prefix(_platform)
		code_server_paste(filename)
		result = workspace_file_exist(filename)
	return DetailsData(
		data=result).json()

def main(platform=None, config=None):
	global _platform, _config
	if platform:
		_platform = platform
	else:
		_platform = get_platform()
	_config = config
	from hi.vm.vbox import vbox_add_path
	vbox_add_path(platform, config)
	run(host='localhost', port=8080)

if __name__ == '__main__':
	main()