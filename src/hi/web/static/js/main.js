navOptions = [ /** shortcut: char, title: string, action: url,shortcut */
	['I', 'Install vm', '/hi.sys/install/vm'],
	['n', 'Install coreos', '/hi.sys/install/coreos'],
	['S', 'Start coreos', '/hi.sys/start/coreos'],
	['t', 'Stop coreos', '/hi.sys/stop/coreos'],
	['D', 'Delete coreos', '/hi.sys/delete/coreos'],
	['z', 'Start monitor server', '/hi.sys/dms/start'],
	['k', 'Start code server', '/hi.sys/code/server/start'],
	['M', 'Navigation Menu', 'm', true],
	['o', 'Prompt Ollama', 'o', true],
	['p', 'Prompt AI', 'p', true]
]
htmlNav = `<nav class="tui-sidenav absolute"><ul id="js-navlist"></ul></nav>
<nav class="tui-nav absolute"><span class="tui-datetime" data-format="h:m:s a">--:--:-- -M</span><ul>
	<li class="tui-sidenav-button red-168-text">≡</li>
</ul></nav>`
htmlModal = `<div class="center"><div id="js-modal" class="tui-modal active">
	<div class="tui-window red-168">
	<fieldset class="tui-fieldset">
		<legend id="js-modal-title" class="red-255 yellow-255-text"></legend>
		<div id="js-modal-detail" class="left-align"></div>
		<button id="js-modal-close" class="tui-button tui-modal-close-button right">close</button>
	</fieldset>
	</div>
</div></div>`
htmlEdit = `<div class="tui-window full-width tui-no-shadow" style="margin-top: 20px;">
	<fieldset class="tui-fieldset">
		<legend class="center">Algorithm Interface(AI)</legend>
		<textarea id="js-edit" class="tui-textarea full-width" style="overflow: scroll;"></textarea>
	</fieldset>
</div>
<div class="tui-statusbar absolute"><ul></ul></div>`
c = {} // conf
c.light = `cyan-white`
c.dark = `blue-black`
c.theme = 'light' // or 'dark'
c.bodyClass=`tui-bg-${c[c.theme]}`
g = window.g = {}
$b = $('body').addClass(c.bodyClass)
function openModal(title, detail, noClose) {
	$modal = $(htmlModal)
	$modal.find('#js-modal-title').text(title)
	$modal.find('#js-modal-detail').html(detail)
	if (noClose) {
		$modal.find('#js-modal-close').hide()
	} else {
		$modal.find('#js-modal-close').click(function() {
			$modal.remove()
		})
	}
	$b.append($modal)
	return $modal
}
g.openModal = openModal
$n=$(htmlNav).appendTo($b)
$e=$(htmlEdit).appendTo($b)
$ta=$('#js-edit')
function setHeight() {
	$ta.height($(window).height() - 102)
}
setHeight()
$(window).resize(setHeight)
function promptAi2Code(cb) {
	const uri = '/hi.sys/crawl/desktop/ai2code?filename='
	const promptText = $ta.val()
	if (promptText == '') {
		cb({ title: 'Prompt AI', detail: 'Prompt needed.'})
		return
	}
	const lines = promptText.split('\n')
	const line0 = lines[0].toLowerCase()
	let filename = (new Date()).toLocaleString('en-GB', { year: '2-digit', month: '2-digit', day: '2-digit',
		hour: '2-digit', minute: '2-digit', second: '2-digit', hour12: false})
		.replace(/(\d{2})\/(\d{2})\/(\d{2}), (\d{2}):(\d{2}):(\d{2})/, '$3$2$1-$4$5$6') + '.md'
	if (line0.indexOf('filename:') > -1) {
		filename = line0.split('filename:')[1]
	}
	$.post(uri + filename, promptText, function(data) {
		cb({ title: 'Prompt AI', detail: 'Prompt done.'})
	}).fail(function(response) {
		cb({ title: 'Prompt AI', detail: 'Error:' + response.responseText})
	})
}
function promptOllama(cb) {
	const uri = '/hi.sys/crawl/ollama/prompt'
	const promptText = $ta.val()
	if (promptText == '') {
		cb({ title: 'Prompt AI', detail: 'Prompt needed.'})
		return
	}
	const lines = promptText.split('\n')
	const line0 = lines[0].toLowerCase()
	let filename = ''
	if (line0.indexOf('filename:') > -1) {
		filename = '?filename=' +line0.split('filename:')[1]
	}
	$.post(uri + filename, promptText, cb).fail(function(response) {
		cb({ title: 'Prompt AI', detail: 'Error:' + response.responseText})
	})
}
function selectNavOption(name, uri) {
	if (uri[0] == '/') {
		$busyModal = openModal('Running', `${name}...`, true)
		$.get(uri, function(data) {
			$busyModal.remove()
			if (typeof(data) == 'string') data = JSON.parse(data)
			if (data.errors) {
				data.detail += `<br><h4>Error(s):</h4>`
				data.errors.forEach((err, idx) => {
					data.detail += `<br> - ${err.code}: ${err.detail}`
				})
			}
			openModal(data.title, data.detail)
		}).fail(function(jqXHR, textStatus, errorThrown) {
			console.error('Error fetching data:', errorThrown)
			$busyModal.remove()
			openModal('Error', textStatus)
		})
	} else { 
		navKeys[uri]()
	}
}
navKeys = {}
navHtml = ''
$nlist = $('#js-navlist')
$statusBarUl = $('.tui-statusbar').find('ul')
function addStatusBarItem(shortcut, title, cb) {
	const statusBarHtml = `<li><a href="#shortcut-${shortcut}"><span class="red-168-text">ctrl+shift+${shortcut}</span> ${title}</a></li>`
	$statusBarItem = $(statusBarHtml)
	$statusBarItem.click(cb)
	$statusBarItem.appendTo($statusBarUl)
}
function addStatusBarDividers () {
	const divider = `&nbsp;<span class="tui-statusbar-divider"></span>`
	$li = $statusBarUl.find('a').slice(0,-1)
	$li.append(divider)
}
navOptions.forEach((opt, idx) => {
	const shortcut = opt[0]
	const name = opt[1]
	const uri = opt[2]
	const inStatusBar = opt[3]
	const shortcutHtml = `<span class="red-168-text">${shortcut}</span>`
	idx = name.indexOf(shortcut)
	var title = name
	if (idx != -1) { 
		title = name.split(shortcut)[0] + shortcutHtml + name.slice(idx+1) 
	}
	$navOpt = $(`<li><a href="#!">${title}&nbsp;<span class="tui-shortcut">ctrl+shift+${shortcut.toLowerCase()}</span></a></li>`)
	const clickNavOption = function() { selectNavOption(name, uri) }
	navKeys[shortcut.toLowerCase()] = clickNavOption
	$navOpt.click(clickNavOption)
	if (inStatusBar) { 
		addStatusBarItem(shortcut.toLowerCase(), title, clickNavOption) 
	} else { 
		$navOpt.appendTo($nlist) 
	}
})
addStatusBarDividers()
navKeys['p'] = function() {
	$busyModal = openModal('Running', 'Prompt AI...', true)
	promptAi2Code(function(data) {
		$busyModal.remove()
		if (data) openModal(data.title, data.detail)
	})
}
navKeys['o'] = function() {
	$busyModal = openModal('Running', 'Prompt Ollama...', true)
	promptOllama(function(data) {
		if (typeof(data) == 'string') {
			ollamaData = JSON.parse(data)
			taVal = $ta.val() + '\n\n--- ' + ollamaData.data.created_at + '\n\n' + ollamaData.data.response
			$ta.val(taVal)
		} else {
			openModal(data.title, data.detail)
		}
		$busyModal.remove()
	})
}
navKeys['m'] = function() {
	$sideNavButton.click()
}
navKeysKeys = Object.keys(navKeys)
datetimeController()
sidenavController()
$sideNavButton = $('.tui-sidenav-button')
$sideNav = $('.tui-sidenav')
$(document).keydown(function(event) {
	if (event.ctrlKey && event.shiftKey) {
		key = event.key.toLowerCase()
		if (navKeysKeys.includes(key)) {
			navKeys[key]()
			event.preventDefault()
		}
	}
})