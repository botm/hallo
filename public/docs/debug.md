# 🐛 Die gogga

Terwyl dit gerieflik is om 'n "program" te "bootstrap" in [gitlab-ide] en [np++],
moet 'n mens ook erens besef dis masoegisties.

Indien een vensters gebruik raai ek aan u:

```cmd
winget install Git.Git
cd %appdata%\hi
git clone git@gitlab.com:botm/hallo.git
cd hallo\src
%appdata%\python\Scripts\pip.exe install --upgrade --force-reinstall -e .
``` 

Nou kan u die `hi` projek in `explorer.exe %appdata%\hi\hallo` verander
of gebruik [`code-server`](http://localhost:8443/?folder=/config/hi/hallo).

[gitlab-ide]: https://gitlab.com/-/ide/project/botm/hallo/edit/main/-/
[np++]: https://notepad-plus-plus.org/

<!-- iso: afr -->