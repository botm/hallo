# Schema's

## Data Model Generation

https://docs.pydantic.dev/1.10/datamodel_code_generator/

```sh
pip install datamodel-code-generator
# installs(pydantic already in env)...
# genson, platformdirs, pathspec, mypy-extensions, MarkupSafe, isort, inflect, dnspython, argcomplete, jinja2, email-validator, black, datamodel-code-generator
pip install datamodel-code-generator[http]
# allow --url parameter
%appdata%\python\Scripts\datamodel-codegen --input-file-type openapi --output-model-type pydantic_v2.BaseModel --disable-timestamp --url https://app.swaggerhub.com/apiproxy/domains/smartbear-public/ProblemDetails/1.0.0 --output problem_details.py
```