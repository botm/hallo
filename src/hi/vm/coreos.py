import re
import subprocess
import sys
import json
import logging
from pathlib import Path

from ..sys.platform import exec_subprocess
from .ssh import create_ssh_key, ssh_key_path
from .vbox import (download_url_text,
	vbox_import, vbox_ignite, vbox_forward_ssh,
	vbox_start, vbox_poweroff, vbox_delete, 
	download_vbox_disk)

logger = logging.getLogger(__name__)

coreos_ign_dict = {
	"ignition": { "version": "3.1.0" },
	"passwd": {
		"users": [ {
			"name": "core",
			"sshAuthorizedKeys": []
		} ]
	}
}

def parse_stable_json(json_text):
	try:
		data = json.loads(json_text)
		vbox_disk_location = data.get("architectures", {}).get("x86_64", {}).get("artifacts", {}).get("virtualbox", {}).get("formats", {}).get("ova", {}).get("disk", {}).get("location")
		if vbox_disk_location:
			logger.debug(f"VirtualBox disk location: {vbox_disk_location}")
			return vbox_disk_location
		else:
			logger.error("VirtualBox disk location not found in stable.json")
			return None
	except json.JSONDecodeError as e:
		logger.error(f"Error parsing stable.json: {e}")
		return None

def download_coreos(platform=None, force=False):
	prefix = '.' if platform is None else platform['config.boot.path']
	stable_json_url = "https://builds.coreos.fedoraproject.org/streams/stable.json"
	vbox_disk_output_file = f"{prefix}/fedora_coreos.ova"
	if Path(vbox_disk_output_file).is_file():
		logger.info(f'{vbox_disk_output_file} exist, skipping download...')
		return vbox_disk_output_file
	json_text = download_url_text(stable_json_url)
	if json_text:
		vbox_disk_location = parse_stable_json(json_text)
		if vbox_disk_location:
			download_vbox_disk(vbox_disk_location, vbox_disk_output_file)
	return vbox_disk_output_file

def start_coreos(platform=None, config=None):
	vm_name = "fedora_coreos"
	return vbox_start(vm_name)

def stop_coreos(platform=None, config=None):
	vm_name = "fedora_coreos"
	return list(vbox_poweroff(vm_name))

def delete_coreos(platform=None, config=None):
	vm_name = "fedora_coreos"
	result = list(vbox_poweroff(vm_name))
	from time import sleep
	sleep(3)
	result.append(vbox_delete(vm_name))
	return result

def install_coreos(platform=None, config=None):
	vm_name = "fedora_coreos"
	ova_file = download_coreos(platform)
	result = list(vbox_import(vm_name, ova_file))
	ign_path = Path(ova_file).with_suffix('.ign')
	if ign_path.is_file():
		with open(ign_path, 'r') as f:
			ign_conf = f.read()
	else:
		logger.warning(f'No ignition file at {ign_path}, falling back to default config with setting core user ssh pub key.')
		public_key_content = create_ssh_key()
		coreos_ign_dict["passwd"]["users"][0]["sshAuthorizedKeys"] = [] # make sure we only have one key if called from http api
		coreos_ign_dict["passwd"]["users"][0]["sshAuthorizedKeys"].append(public_key_content)
		ign_conf = json.dumps(coreos_ign_dict)
	result.append(vbox_ignite(vm_name, ign_conf))
	result.append(vbox_forward_ssh(vm_name))
	# TODO find a better place to init these vbox items
	from hi.code import code_server_forward_https, code_server_share_hi_data
	result.append(code_server_forward_https(vm_name))
	result.append(code_server_share_hi_data(vm_name, platform))
	return result

def remove_known_hosts_line(dotssh_path: str, linenr: int):
	known_hosts_file = f"{dotssh_path}/known_hosts"
	try:
		with open(known_hosts_file, 'r') as f:
			lines = f.readlines()
		with open(known_hosts_file, 'w') as f:
			for i, line in enumerate(lines):
				if i + 1 != linenr:
					f.write(line)
				else:
					logger.debug(f'Removing line from {known_hosts_file}:{linenr} = {line}')
	except FileNotFoundError:
		logger.warning(f"{known_hosts_file} not found.")
		pass
		
def exec_subprocess_ssh(cmds):
	result = []
	for cmd in cmds:
		if cmd.strip() == '':
			continue
		ssh_cmd = ['ssh', '-o', 'StrictHostKeyChecking=accept-new', 'core@localhost', '-p', '2223', '-C', cmd]
		output = exec_subprocess(ssh_cmd)
		if 'REMOTE HOST IDENTIFICATION HAS CHANGED' in output:
			logger.warning('Trying to remove old hostkey from known_hosts')
			offend_line_regex = r'Offending .* key in (.+)/known_hosts:(\d+)'
			match_result = re.search(offend_line_regex, output)
			if match_result:
				dotssh_path, linenr = match_result.groups()
				# Remove the offending line using py
				remove_known_hosts_line(dotssh_path, int(linenr))
				# Retry the SSH command
				output = exec_subprocess(ssh_cmd)
		result.append(output)
	return result

def exec_ssh(cmds):
	private_key_path = ssh_key_path()
	#return exec_netmiko_ssh('localhost', '2223', 'core', private_key_path, cmds)
	return exec_paramiko_ssh('localhost', '2223', 'core', private_key_path, cmds)

def exec_netmiko_ssh(server, port, username, private_key_path, commands):
	from netmiko import ConnectHandler
	device = {
		"device_type": "linux",
		"host": server,
		"port": port,
		"username": username,
		"use_keys": True,
		"key_file": private_key_path,
	}
	output_list = []
	try:
		net_connect = ConnectHandler(**device)
		for command in commands:
			if command.strip() == '':
				continue
			logger.debug(command)
			net_connect.write_channel(command)
			output = net_connect.read_until_prompt(0)
			output_list.append(output)
	except:
		logger.exception(command)
	finally:
		net_connect.disconnect()
	return output_list

def exec_paramiko_ssh(server, port, username, private_key_path, commands):
	import paramiko
	output_list = []
	try:
		key = paramiko.RSAKey(filename=private_key_path)
		client = paramiko.SSHClient()
		client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
		client.connect(server, port=port, username=username, pkey=key)
		for command in commands:
			if command.strip() == '':
				continue
			stdin, stdout, stderr = client.exec_command(command)
			stdout.channel.recv_exit_status()
			output_list.append(stdout.read().decode('utf-8'))
			output_list.append(stderr.read().decode('utf-8'))
	except paramiko.SSHException as e:
		print(f"Error executing SSH command: {e}")
	finally:
		client.close()
	return output_list