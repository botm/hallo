import logging

from hi.sys.platform import exec_subprocess
from hi.vm.coreos import exec_subprocess_ssh, exec_ssh

logger = logging.getLogger(__name__)

podman_network = 'monitor'

postgres_image = 'docker.io/postgres:latest'
zabbix_image = 'docker.io/zabbix/zabbix-server-pgsql:latest'
zabbix_web_image = 'docker.io/zabbix/zabbix-web-nginx-pgsql:latest'
zabbix_agent_image = 'docker.io/zabbix/zabbix-agent:latest'

zabbix_db_name = 'some-postgres'
zabbix_db_pass = 'mysecretpassword'

zabbix_server_name = 'some-zabbix-server-pgsql'
zabbix_web_server_name = 'some-zabbix-web-nginx-pgsql'
zabbix_agent_name = 'some-zabbix-agent'
zabbix_web_server_tz = 'Africa/Harare'

def build_podman_zabbix_cmds():
	podman_zabbix_cmds = f'''podman network create {podman_network}
podman run --net {podman_network} --name {zabbix_db_name} -e POSTGRES_PASSWORD={zabbix_db_pass} -d {postgres_image}
sleep 3
podman exec {zabbix_db_name} createdb -U postgres zabbix
podman run --net {podman_network} --name {zabbix_server_name} -e DB_SERVER_HOST="{zabbix_db_name}" -e POSTGRES_USER="postgres" -e POSTGRES_PASSWORD="{zabbix_db_pass}" --init -d {zabbix_image}
podman run --net {podman_network} -p 8080:8080 -p 8443:8443 --name {zabbix_web_server_name} -e DB_SERVER_HOST="{zabbix_db_name}" -e POSTGRES_USER="postgres" -e POSTGRES_PASSWORD="{zabbix_db_pass}" -e ZBX_SERVER_HOST="{zabbix_server_name}" -e PHP_TZ="{zabbix_web_server_tz}" -d {zabbix_web_image}
podman run --net {podman_network} --name {zabbix_agent_name} -e ZBX_HOSTNAME="{zabbix_agent_name}" -e ZBX_SERVER_HOST="{zabbix_server_name}" --init -d {zabbix_agent_image}
'''
	return podman_zabbix_cmds

def start_server():
	cmds = build_podman_zabbix_cmds().split('\n')
	try:
		#return exec_ssh(cmds)
		return exec_subprocess_ssh(cmds)
	except:
		logger.exception(cmds)
		#return exec_subprocess_ssh(cmds)

def connect_server_api(url="127.0.0.1:8081",user="Admin", password="zabbix"):
	from zabbix_utils import ZabbixAPI

	api = ZabbixAPI(url=url)
	api.login(user=user, password=password)
	return api

def build_template_xml(name="Host LLD Template"):
	import zabbix_utils

	# Create a Zabbix template
	template = zabbix_utils.Template(name=name)

	# Create a discovery rule
	discovery_rule = zabbix_utils.DiscoveryRule(
		name="host.discovery",
		key="host.discovery",
		delay=0,
		lifetime="30d",
		description="Discovery rule for hosts",
	)

	# Create an item prototype
	item_prototype = zabbix_utils.ItemPrototype(
		name="{#ITEM_NAME}",
		key="{#ITEM_NAME}",
		type=2,
		value_type=4,
		delay=0,
		history="90d",
	)

	# Create a host prototype
	host_prototype = zabbix_utils.HostPrototype(
		host="{#HOST_NAME}",
		name="{#HOST_NAME}",
		templates=[template],
		group_links=["Templates"],
	)

	# Add item prototype to discovery rule
	discovery_rule.add_item_prototype(item_prototype)

	# Add host prototype to discovery rule
	discovery_rule.add_host_prototype(host_prototype)

	# Add discovery rule to template
	template.add_discovery_rule(discovery_rule)

	# Export the template
	template_xml = template.to_xml()
	return template_xml

def create_template(template_xml, api=None):
	# Create or update the template
	if not api:
		api = connect_server_api()
	try:
		template_id = api.templates.create_or_update(template_xml)
		logger.info(f"Template uploaded successfully! Template ID: {template_id}")
		return template_id
	except:
		logger.exception(template_xml)