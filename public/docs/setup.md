# Setup

## Windows

You can use `cmd.exe` or `powershell.exe` to boot the agent.

For windows(`amd64`) the one-liner install
python3 embed, pip3 setuptools,
with `hi.sys` agent
into the user roaming directory,
and add a cron task running every minute.

### Using `powershell.exe`

##### Install

```ps1
powershell -c "[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12; Invoke-WebRequest -Uri 'https://gitlab.com/botm/hallo/-/raw/main/scripts/embed-amd64.ps1' -OutFile $env:appdata\install-hi.ps1; cd $env:appdata; powershell .\install-hi.ps1"
```

#### Execute

```ps1
& $env:appdata\python\python.exe -m hi.sys
```

### Using `command.exe`

#### Install

```cmd
powershell -c "[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12; Invoke-WebRequest -Uri 'https://gitlab.com/botm/hallo/-/raw/main/scripts/embed-amd64.ps1' -OutFile '%appdata%\install-hi.ps1'; cd %appdata%; powershell .\install-hi.ps1"
```

#### Execute

```cmd
%appdata%\python\python.exe -m hi.sys
```

### Ollama

A Ollama server need to run on `localhost` for Ollama prompting to work.

Download and install the [Ollama Server(Windows)](https://ollama.com/download/windows)
and follow the [Quickstart](https://github.com/ollama/ollama/blob/main/README.md#quickstart)
to install a modal.

### AI Prompt

AI prompting use desktop crawling,
this need third party packages
`pip install pyautogui pillow opencv-python`.

Images to navigate the desktop should be created in `%appdata%\hi\crawl\desktop`:

```
ai-copy.png
ai-prompt.png
ai-stop.png
ai-submit.png
ai-textarea.png
ai.png
browser-open-tab.png
browser-visible.png
code-server-file-new.png
code-server-file.png
code-server-menu.png
code-server.png
taskbar-browser.png
```

## Other

Other operating systems use the [Hello Package Registry](https://gitlab.com/botm/hallo/-/packages/26618710)
to install the pip package

```
pip install hi-<version>.tar.gz
```

and add the following to cron:

```
* * * * * python -m hi.sys.cron
```