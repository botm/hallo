from .problem_details import ProblemDetails, ErrorDetail

from pydantic import AnyUrl

class Details(ProblemDetails):
	type: AnyUrl = "https://gitlab.com/botm/hallo/-/blob/main/public/docs/api.md#OK"
	status: int = 200
	title: str = "Ok"
	detail: str = "Successful requests"

class DetailsData(Details):
	data: object = None