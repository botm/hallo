import sys
import logging

from .monitor import connect_server_api, zabbix_agent_name, build_template_xml, create_template

logger = logging.getLogger(__name__)

def main():
	api = connect_server_api()
	users = api.user.get(
		output=['userid','name']
	)

	for user in users:
		user_name = user['name']
		logger.info(user_name)
		print(user_name)

	hosts = api.host.get(
		output=['hostid','name']
	)
	for host in hosts:
		logger.info(host)
		print(host)
		if 'Zabbix server' in host['name']:
			result = api.host.update(hostid=host['hostid'], name=zabbix_agent_name)
			logger.info(result)
			result = api.host.update(hostid=host['hostid'], hostInterfaces=[dict(type=1,main=1,useip=0,ip='',dns=zabbix_agent_name,port=10050)])
			logger.info(result)

	templates = api.template.get(
		output=['templateid','name']
	)
	host_template_found = False
	for template in templates:
		logger.info(template)
		print(template)
		if 'Host LLD Template' in template['name']:
			host_template_found = True
	if not host_template_found:
		xml = build_template_xml()
		template_id = create_template(xml, api)
		print(f'Created Host LLD Template as {template_id}')
	api.logout()
	return 0

if __name__ == '__main__':
	sys.exit(main())