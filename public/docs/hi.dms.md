# Disturbed Monitoring System (DMS)

- Server
- Proxy
- Agent

### The Four Golden Signals for Monitoring Distributed Systems

Monitoring distributed systems is crucial for maintaining reliability and performance. Google’s Site Reliability Engineering (SRE) teams have identified four key metrics, known as the “four golden signals,” that provide valuable insights into system health:

1. Latency: Latency measures the time it takes for a request to travel through the system. It’s essential to monitor latency because slow responses can impact user experience and overall system performance. High latency may indicate bottlenecks or resource constraints.

2. Traffic: Monitoring traffic helps you understand the load on your system. It includes metrics like request rates, data transfer, and concurrent connections. Sudden spikes in traffic can strain resources and lead to performance issues.

3. Errors: Tracking error rates is critical for identifying issues. Errors can occur due to software bugs, misconfigurations, or external dependencies. High error rates may impact user satisfaction and system stability.

4. Saturation: Saturation refers to resource utilization. It measures how close a system is to its capacity limits. Monitoring CPU, memory, disk, and network usage helps prevent resource exhaustion and performance degradation.

Remember, if you can only measure four metrics, focus on these four golden signals—they provide essential insights into your system’s behavior and health.