// From https://raw.githubusercontent.com/vinibiavatti1/TuiCss/master/src/js/tuicss.js transpiled for Windows.Forms.WebBrowser(IE) using https://babeljs.io/repl
function _createForOfIteratorHelper(r, e) { var t = "undefined" != typeof Symbol && r[Symbol.iterator] || r["@@iterator"]; if (!t) { if (Array.isArray(r) || (t = _unsupportedIterableToArray(r)) || e && r && "number" == typeof r.length) { t && (r = t); var _n = 0, F = function F() {}; return { s: F, n: function n() { return _n >= r.length ? { done: !0 } : { done: !1, value: r[_n++] }; }, e: function e(r) { throw r; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var o, a = !0, u = !1; return { s: function s() { t = t.call(r); }, n: function n() { var r = t.next(); return a = r.done, r; }, e: function e(r) { u = !0, o = r; }, f: function f() { try { a || null == t["return"] || t["return"](); } finally { if (u) throw o; } } }; }
function _unsupportedIterableToArray(r, a) { if (r) { if ("string" == typeof r) return _arrayLikeToArray(r, a); var t = {}.toString.call(r).slice(8, -1); return "Object" === t && r.constructor && (t = r.constructor.name), "Map" === t || "Set" === t ? Array.from(r) : "Arguments" === t || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(t) ? _arrayLikeToArray(r, a) : void 0; } }
function _arrayLikeToArray(r, a) { (null == a || a > r.length) && (a = r.length); for (var e = 0, n = Array(a); e < a; e++) n[e] = r[e]; return n; }
/**
 * Replacement for jQuery's $(document).ready() function.
 * This is handy in making sure stuff fires after the DOM is ready to be touched.
 * Stolen from:https://stackoverflow.com/a/53601942/344028
 *
 * @param fn Callback.
 */
function domReady(fn) {
  // If we're early to the party
  document.addEventListener('DOMContentLoaded', fn);

  // If late; I mean on time.
  if (document.readyState === 'interactive' || document.readyState === 'complete') {
    fn();
  }
}

/**
 * TuiTabs controller
 */
function tabsController() {
  // Get all the tab elements (typically li tags).
  var tabs = document.getElementsByClassName('tui-tab');
  if (!tabs.length) {
    // No tabs found, return early and save a couple CPU cycles.
    return;
  }
  var _iterator = _createForOfIteratorHelper(tabs),
    _step;
  try {
    for (_iterator.s(); !(_step = _iterator.n()).done;) {
      var tab = _step.value;
      // Add click listeners to them.
      tab.addEventListener('click', function (e) {
        // Check if the clicked tab is disabled
        if (e.target.classList.contains("disabled")) {
          return;
        }

        // Remove the 'active' class from any and all tabs.
        var _iterator2 = _createForOfIteratorHelper(tabs),
          _step2;
        try {
          for (_iterator2.s(); !(_step2 = _iterator2.n()).done;) {
            var otherTab = _step2.value;
            otherTab.classList.remove('active');
          }

          // Get the content element.
        } catch (err) {
          _iterator2.e(err);
        } finally {
          _iterator2.f();
        }
        var tabContents = document.getElementsByClassName('tui-tab-content');
        if (tabContents) {
          var _iterator3 = _createForOfIteratorHelper(tabContents),
            _step3;
          try {
            for (_iterator3.s(); !(_step3 = _iterator3.n()).done;) {
              var tabContent = _step3.value;
              // Hide all tab contents.
              tabContent.style.display = 'none';
            }
          } catch (err) {
            _iterator3.e(err);
          } finally {
            _iterator3.f();
          }
        } else {
          throw 'No tab content elements found.';
        }

        // Get the id of the tab contents we want to show.
        var tabContentId = e.target.getAttribute('data-tab-content');
        if (tabContentId) {
          var _tabContent = document.getElementById(tabContentId);
          if (_tabContent) {
            // Show the tab contents.
            _tabContent.style.display = 'block';
          } else {
            throw 'No tab content element with id "' + tabContentId + '" found.';
          }
        }
        // We are not going to throw an error here, since we could make the tab do something else that a tab
        // normally wouldn't do.

        // Set the clicked tab to have the 'active' class so we can use it in the next part.
        e.target.classList.add('active');
      });
    }

    // Grab the first tab with the active class.
  } catch (err) {
    _iterator.e(err);
  } finally {
    _iterator.f();
  }
  var activeTab = document.querySelector('.tui-tab.active');
  if (activeTab) {
    // Now click it 'click' it.
    activeTab.click();
  } else {
    // Nothing found, just click the first tab foud.
    tabs[0].click();
  }
}

/**
 * Date/time field controller
 */
function datetimeController() {
  // Get date/time elements.
  var clocks = document.getElementsByClassName('tui-datetime');
  if (!clocks.length) {
    // No date time elements found, return early and save a couple CPU cycles.
    return;
  }

  // Kick off our clock interval stuff.
  datetimeInterval();

  // Synchronize time and set interval to control the clocks
  setTimeout(function () {
    setInterval(datetimeInterval, 1000);
  }, 1000 - new Date().getMilliseconds());
  function datetimeInterval() {
    var _iterator4 = _createForOfIteratorHelper(clocks),
      _step4;
    try {
      for (_iterator4.s(); !(_step4 = _iterator4.n()).done;) {
        var clock = _step4.value;
        if (clock === null) {
          continue;
        }

        // Get the format we want to display in the element.
        var format = clock.getAttribute('data-format');

        // parse out the date and time into constants.
        var today = new Date();
        var month = (today.getMonth() + 1).toString().padStart(2, '0');
        var day = today.getDate().toString().padStart(2, '0');
        var dayOfWeek = (today.getDay() + 1).toString().padStart(2, '0');
        var year = today.getFullYear().toString();
        var hour = today.getHours().toString().padStart(2, '0');
        var hour12 = (parseInt(hour) + 24) % '12' || '12';
        var minute = today.getMinutes().toString().padStart(2, '0');
        var second = today.getSeconds().toString().padStart(2, '0');
        var ampm = parseInt(hour) >= 12 ? 'PM' : 'AM';

        // Replace based on the format.
        format = format.replace('M', month);
        format = format.replace('d', day);
        format = format.replace('e', dayOfWeek);
        format = format.replace('y', year);
        format = format.replace('H', hour);
        format = format.replace('h', hour12);
        format = format.replace('m', minute);
        format = format.replace('s', second);
        format = format.replace('a', ampm);

        // Show it in the element.
        clock.innerHTML = format;
      }
    } catch (err) {
      _iterator4.e(err);
    } finally {
      _iterator4.f();
    }
  }
}

/**
 * Sidenav Controller
 * There should only side navigation element at the moment.
 */
function sidenavController() {
  // Get the side navigation button (there should be only one, but if not, we are getting the first one).
  var sideNavButton = document.querySelector('.tui-sidenav-button');
  if (!sideNavButton) {
    // No side navigation button found, return early and save a couple CPU cycles.
    return;
  }

  // Add the click event listener to the buttons.
  sideNavButton.addEventListener('click', function () {
    // Get the side navigation element (there should be only one, but if not, we are getting the first one).
    var sideNav = document.querySelector('.tui-sidenav');
    if (sideNav) {
      if (sideNav.classList.contains('active')) {
        sideNav.classList.remove('active');
      } else {
        sideNav.classList.add('active');
      }
    } else {
      throw 'No sidenav element found.';
    }
  });
}

/**
 * Modal controller
 */
function modalController() {
  // Get the overlap (overlay) element (there should be only one, but if not, we are getting the first one).
  var tuiOverlap = document.querySelector('.tui-overlap');
  if (!tuiOverlap) {
    // No overlap found element, return early and save a couple CPU cycles.
    return;
  }

  // Find modal buttons.
  var modalButtons = document.getElementsByClassName('tui-modal-button');
  var _iterator5 = _createForOfIteratorHelper(modalButtons),
    _step5;
  try {
    for (_iterator5.s(); !(_step5 = _iterator5.n()).done;) {
      var modalButton = _step5.value;
      // Add the click event listener to the buttons.
      modalButton.addEventListener('click', function (e) {
        // Show the overlap.
        tuiOverlap.classList.add('active');

        // Get the display element for the modal.
        var modalId = e.target.getAttribute('data-modal');
        if (modalId) {
          var modal = document.getElementById(modalId);
          if (modal) {
            // Show it.
            modal.classList.add('active');
          } else {
            throw 'No modal element with id of "' + modalId + '" found.';
          }
        } else {
          throw 'Modal close button data-modal attribute is empty or not set.';
        }
      });
    }

    // Find the close modal buttons.
  } catch (err) {
    _iterator5.e(err);
  } finally {
    _iterator5.f();
  }
  var modalCloseButtons = document.getElementsByClassName('tui-modal-close-button');
  if (modalButtons.length > 0 && !modalCloseButtons.length) {
    // A modal without a close button, is a bad modal.
    throw 'No modal close buttons found.';
  }
  var _iterator6 = _createForOfIteratorHelper(modalCloseButtons),
    _step6;
  try {
    for (_iterator6.s(); !(_step6 = _iterator6.n()).done;) {
      var modalCloseButton = _step6.value;
      // Add the click event listener to the buttons.
      modalCloseButton.addEventListener('click', function (e) {
        // Hide the the overlap.
        tuiOverlap.classList.remove('active');

        // Get the display element id for the modal.
        var modalId = e.target.getAttribute('data-modal');
        if (modalId) {
          // Get the modal element.
          var modal = document.getElementById(modalId);
          if (modal) {
            // Hide it.
            modal.classList.remove('active');
          } else {
            throw 'No modal element with id of "' + modalId + '" found.';
          }
        } else {
          throw 'Modal close button data-modal attribute is empty or not set.';
        }
      });
    }
  } catch (err) {
    _iterator6.e(err);
  } finally {
    _iterator6.f();
  }
}

/**
 * Init: This is at the bottom to make sure it is fired correctly.
 
domReady(function () {
  tabsController();
  datetimeController();
  sidenavController();
  modalController();
});
*/