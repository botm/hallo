# hallo

## Getting started

### Windows

For windows(`amd64`) the powershell one-liner
install python3 embed, pip3 setuptools,
with `hi.sys` agent
into the user roaming directory,
add a cron task running every minute,
and finally start up a localhost web app
to interact with Hallo Systems.

```ps1
irm https://gitlab.com/botm/hallo/-/raw/main/scripts/embed-amd64.ps1 | iex
```

The web app can also be started from the run dialog(`<win>+r`):

```cmd
%appdata%\python\python.exe -m hi.sys
```

**The above effectively allowing the agent to execute management remote procedure calls(mgmt-rpc) every minute.**

## Disclosure

Defaults use non SSL protocol to allow debugging at the network layer. 
For production, deploy a `config.ini` file with a secure `boot.url` 
or change the `fallback_url` in the agent before mass deployment.

## Other platforms and setup of ai components

See [setup](public/docs/setup.md).